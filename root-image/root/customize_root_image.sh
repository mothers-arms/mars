#!/bin/bash

set -e -u

sed -i 's/#\(en_GB\.UTF-8\)/\1/' /etc/locale.gen
locale-gen

ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime

cp -aT /etc/skel/ /root/

useradd -m -p "aakYhRHG8z/3o" -g users -G "adm,audio,floppy,log,network,rfkill,scanner,storage,optical,power,wheel" -s /bin/bash ma

chmod 750 /etc/sudoers.d
chmod 440 /etc/sudoers.d/g_wheel

sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist
sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf

sed -i "s/#ListenAddress/ListenAddress/" /etc/ssh/sshd_config

#localectl set-keymap --no-convert uk

localepurge-config
sed -i "s/NEEDSCONFIGFIRST/#NEEDSCONFIGFIRST/" /etc/locale.nopurge
localepurge

mandb -c

systemctl enable multi-user.target sshd.service
