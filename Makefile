all: mars

mars: out/MARS-$(shell date +%F)-i686.iso
out/MARS-$(shell date +%F)-i686.iso:
	./build.sh

# In case "make clean" is called, the following routine gets rid of all files created by this Makefile.
clean:
	rm -rf "work" "out"
	rm base-packages.list
	find . -name "*~" -delete

.PHONY: all mars
.PHONY: clean

