#!/bin/bash

set -e -u

name=MARS
iso_label="MARS_$(date +%Y%m)"
iso_version=$(date +%F)
install_dir=mars
arch=$(uname -m)
work_dir=work
out_dir=out
verbose="y"
# Generate package list from base group:
pacman -Sg base | sed -e 's/base //' > base-packages.list
# Then merge with
packages="$(cat base-packages.list packages.list \
	| sed -e 's/^linux$//' \
	| sort -u)"
publisher="Mother's Arms <http://www.mothers-arms.co.uk/>"
application="Mother's Arms Rescue System"

script_path=$(readlink -f ${0%/*})

# Helper function to run make_*() only one time per architecture.
run_once() {
    if [[ ! -e ${work_dir}/build.${1}_${arch} ]]; then
        $1
        touch ${work_dir}/build.${1}_${arch}
    fi
}

# Base installation (root-image)
make_basefs() {
    echo "making base install"
    # Shouldn't need to do init as mkarchiso calls pacstrap which creates needed dirs for us.
    #mkdir -p ${work_dir}/root-image/{dev,proc,run,sys,tmp,var/lib/pacman} # Borrowed from mkarchiso, since we want to do our own init
    mkarchiso ${verbose} -w "${work_dir}" -C pacman.conf -D "${install_dir}" -p "${packages}" install
 }

# Customize installation (root-image)
make_customize_root_image() {
	echo "Installing local ABS packages"
	pacman --noconfirm --root ${work_dir}/root-image -S linux-lts --noscriptlet
	echo "Copying overlay"
        cp -af root-image ${work_dir}
	mkarchiso ${verbose} -w "${work_dir}" -C pacman.conf -D "${install_dir}" -r '/root/customize_root_image.sh' run
	rm ${work_dir}/root-image/root/customize_root_image.sh
}

# Copy mkinitcpio archiso hooks (root-image)
make_setup_mkinitcpio() {
        echo "copying setup hooks"
        cp -a /lib/initcpio/hooks/* ${work_dir}/root-image/lib/initcpio/hooks
        cp -a /lib/initcpio/install/* ${work_dir}/root-image/lib/initcpio/install
        cp /lib/initcpio/archiso_shutdown ${work_dir}/root-image/lib/initcpio/
        cp ${script_path}/mkinitcpio.conf ${work_dir}/root-image/etc/mkinitcpio-marsiso.conf
	# Running depmod here as we skipped the package install script
	mkarchiso ${verbose} -w "${work_dir}" -C pacman.conf -D "${install_dir}" -r 'depmod 3.10.30-1-lts' run
	mkarchiso ${verbose} -w "${work_dir}" -C pacman.conf -D "${install_dir}" -r 'mkinitcpio -c /etc/mkinitcpio-marsiso.conf -k /boot/vmlinuz-linux-lts -g /boot/marsiso.img' run
}

# Prepare ${install_dir}/boot/
make_boot() {
        echo "copying boot setup"
        mkdir -p ${work_dir}/iso/${install_dir}/boot/${arch}
        cp -a ${work_dir}/root-image/boot/* ${work_dir}/iso/${install_dir}/boot/${arch}
	mv ${work_dir}/iso/${install_dir}/boot/${arch}/memtest86+/memtest{.bin,} 
	mv ${work_dir}/iso/${install_dir}/boot/${arch}/vmlinuz{-linux-lts,} 
	# Remove source kernel as it's all in the initrd now
	mkarchiso ${verbose} -w "${work_dir}" -C pacman.conf -D "${install_dir}" -r 'pacman --noconfirm -R linux-lts linux-firmware' run
}

# Prepare /${install_dir}/boot/syslinux
make_syslinux() {
        echo "making syslinux"
        mkdir -p ${work_dir}/iso/${install_dir}/boot/syslinux
        sed "s|%ARCHISO_LABEL%|${iso_label}|g;
            s|%INSTALL_DIR%|${install_dir}|g;
            s|%ARCH%|${arch}|g" syslinux/syslinux.cfg > ${work_dir}/iso/${install_dir}/boot/syslinux/syslinux.cfg
        cp ${work_dir}/root-image/usr/lib/syslinux/bios/*.c32 ${work_dir}/iso/${install_dir}/boot/syslinux/
	cp ${work_dir}/root-image/usr/lib/syslinux/bios/lpxelinux.0 ${work_dir}/iso/${install_dir}/boot/syslinux/
	cp ${work_dir}/root-image/usr/lib/syslinux/bios/memdisk ${work_dir}/iso/${install_dir}/boot/syslinux/
}

# Prepare /isolinux
make_isolinux() {
        echo "making isolinux"
        mkdir -p ${work_dir}/iso/isolinux
        sed "s|%INSTALL_DIR%|${install_dir}|g" isolinux/isolinux.cfg > ${work_dir}/iso/isolinux/isolinux.cfg
        cp ${work_dir}/root-image/usr/lib/syslinux/bios/isolinux.bin ${work_dir}/iso/isolinux/
        cp ${work_dir}/root-image/usr/lib/syslinux/bios/isohdpfx.bin ${work_dir}/iso/isolinux/
        cp ${work_dir}/root-image/usr/lib/syslinux/bios/ldlinux.c32 ${work_dir}/iso/isolinux/
}

# Process aitab
make_aitab() {
    echo "making aitab"
        sed "s|%ARCH%|${arch}|g" aitab > ${work_dir}/iso/${install_dir}/aitab
}

# Build all filesystem images specified in aitab (.fs .fs.sfs .sfs)
make_prepare() {
    echo "making squash filesystems (prepare)"
    mkarchiso ${verbose} -w "${work_dir}" -D "${install_dir}" pkglist
    mkarchiso ${verbose} -w "${work_dir}" -D "${install_dir}" prepare
}

# Build ISO
make_iso() {
    echo "final stage, should output an ISO!"
    mkarchiso ${verbose} -w "${work_dir}" -D "${install_dir}" checksum
    mkarchiso ${verbose} -C pacman.conf \
	    -D "${install_dir}" \
	    -L "${iso_label}" \
	    -P "${publisher}" \
	    -A "${application}" \
	    -o "${out_dir}" \
	    -w "${work_dir}" \
	    iso "${name}-${iso_version}-${arch}.iso"
}

if [[ $verbose == "y" ]]; then
    verbose="-v"
else
    verbose=""
fi

mkdir -p ${work_dir}/root-image

run_once make_basefs
run_once make_customize_root_image
run_once make_setup_mkinitcpio
run_once make_boot
run_once make_syslinux
run_once make_isolinux
run_once make_aitab
run_once make_prepare
run_once make_iso
